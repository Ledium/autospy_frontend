<?php
/**
 * Class representing single result item.
 */
namespace App;

class Ad
{
	// Ad ID on vendor's site
	public $id;
	// URL to ad
	public $url;
	// Ad title (make and model)
	public $title;
	// City
	public $city;
	// Image URL
	public $image;
	// Price (without currency symbol)
	public $price;
	// Date of manufacturing (or first registration)
	public $year;
	// Transmission type
	public $gearbox;
	// Fuel type
	public $fuel;
	// Engine capacity
	public $engine;
	// Engine powe (kW or HP)
	public $power;
	// Ad vendor name
	public $vendor;

	/**
	 * Validate and return current Ad object.
	 * 
	 * @return Ad
	 */
	public function validate() {
		// TODO: some validation stuff maybe goes here later
		
		return $this;
	}
}