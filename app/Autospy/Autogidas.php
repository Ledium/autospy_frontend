<?php
/**
 * Content scrapper class for Autogidas.lt
 */
namespace App\Autospy;

use App\Autospy\Driver;

class Autogidas extends Driver
{

	// Vendor filter keys (application key => vendor key)
	protected $key_bindings = [
		'make' 		 => 'f_1',			// Make
		'model' 	 => 'f_model_14',	// Model
		'year_from'  => 'f_41',	    	// Year of manufacture (from)
		'year_to' 	 => 'f_42',			// Year of manufacture (to)
		'price_from' => 'f_215',		// Price (from)
		'price_to' 	 => 'f_216',		// Price (to)
		'fuel' 		 => 'f_2',			// Fuel type
		'text' 		 => 'f_376',		// Text search
		'age' 		 => 'f_60',			// Age of item (i.e. not older than 2 days)
		'order' 	 => 'f_50',			// Sort by (i.e. price, date created)
	];

	// Vendor filter values (application value => vendor value)
	protected $value_bindings = [
		'age' => [
			'Rodyti visus' 				=> 728,
			'Šios dienos (tik nauji)' 	=> 6315,
			'Šios dienos' 				=> 729,
			'Vakar dienos' 				=> 2223,
			'Trijų dienų' 				=> 2224,
			'Savaitės' 					=> 731,
			'Dviejų savaičių' 			=> 732
		],
		'order' => [
			'Naujausi viršuje' 	 => 'atnaujinimo_laika_asc',
			'Pigiausi viršuje' 	 => 'kaina_asc',
			'Brangiausi viršuje' => 'kaina_desc'
		],
	];

	// Vendor name
	public $vendor = 'autogidas';

	// Vendor search query URL
	protected $search_url = 'https://autogidas.lt/automobiliai/';

	// Vendor car body type values
	private $body_types = [
		'Sedanas',
		'Hečbekas',
		'Universalas',
		'Visureigis',
		'Vienatūris',
		'Coupe',
		'Kabrioletas',
		'Keleivinis mikroautobusas',
		'Kombi mikroautobusas',
		'Krovininis mikroautobusas',
		'Komercinis auto(su būda)',
		'Pikapas',
		'Limuzinas',
		'Savadarbis auto',
	];

	/**
	 * Create Ad object from single result node.
	 * 
	 * @param  DOMElement $node Single crawled DOM element
	 * @return Ad 				Standardized result object
	 */
	protected function extractData($node) {
		
		// Initialize new Ad object
		$item = new \App\Ad;

		$item->vendor = $this->vendor;
		$item->url = 'https://autogidas.lt' . $node->attr('href');
		$item->id = $node->filter('.ad')->first()->attr('data-ad-id');
		$item->city = $node->filter('.city')->first()->text();
		$item->image = $node->filter('.image img')->first()->attr('src');
		$price = str_replace('€', '', $node->filter('.item-price')->first()->text());
		$item->price = preg_replace('/\s+/', '', $price);

		// Clean title from body type information
		$title = $node->filter('.item-title')->first()->text();
		foreach($this->body_types as $body) {
			$title = str_replace($body, '', $title);
		}
		$item->title = rtrim($title, ' ');
		
		// Parse multiple values from string
		$merged = $node->filter('.item-description .primary')->first()->text();
		$merged = explode(', ', $merged);
		$item->year = isset($merged[0]) ? $merged[0] : null;
		$item->gearbox = isset($merged[1]) ? $merged[1] : null;

		// Parse multiple values from string
		$merged = $node->filter('.item-description .secondary')->first()->text();
		$merged = explode(', ', $merged);
		$item->fuel = isset($merged[0]) ? $merged[0] : null;;
		$item->engine = isset($merged[1]) ? $merged[1] : null;
		$item->power = isset($merged[2]) ? $merged[2] : null;

		// $this->log->addInfo('Item: ' . json_encode($item));

		return $item->validate();
	}

	
}