<?php
/**
 * Options helper for storing search filter values.
 * Car make and model values are stored in DB.
 * 
 */
namespace App;

use App\CarMake;
use App\CarModel;

class Options
{
    // Car makes
	protected $makes;    

    // Years of manufacturing range (static)
    protected $years = [
                1989 => 1989,
                1988 => 1988,
                1987 => 1987,
                1986 => 1986,
                1985 => 1985,
                1980 => 1980,
                1975 => 1975,
                1970 => 1970,
                1965 => 1965,
                1960 => 1960,
                1950 => 1950,
                1940 => 1940,
                1930 => 1930,
                1927 => 1927,
                1925 => 1925,
            ];

    // Price range
    protected $prices = [
                '' => '',
                0 => 0,
                150 => 150,
                300 => 300,
                500 => 500,
                1000 => 1000,
                1500 => 1500,
                2000 => 2000,
                2500 => 2500,
                3000 => 3000,
                3500 => 3500,
                4000 => 4000,
                4500 => 4500,
                5000 => 5000,
                6000 => 6000,
                7000 => 7000,
                8000 => 8000,
                9000 => 9000,
                10000 => 10000,
                12500 => 12500,
                15000 => 15000,
                17500 => 17500,
                20000 => 20000,
                25000 => 25000,
                30000 => 30000,
                45000 => 45000,
                60000 => 60000,
            ];

    // Fuel type
    protected $fuel = [

                '' => '',
                'Dyzelinas' => 'Dyzelinas',
                'Benzinas' => 'Benzinas',
                'Benzinas/Dujos' => 'Benzinas/Dujos',
                'Benzinas/Elektra' => 'Benzinas/Elektra',
                'Elektra' => 'Elektra',
                'Dyzelinas/Elektra' => 'Dyzelinas/Elektra',
                'Dujos' => 'Dujos',
                'Benzinas/Gamtinės dujos' => 'Benzinas/Gamtinės dujos',
                'Etanolis' => 'Etanolis',
                'Kitas' => 'Kitas',
            ];
            
    // Age of ads
    protected $age = [
			    'Rodyti visus' => 'Rodyti visus',
			    'Šios dienos (tik nauji)' => 'Šios dienos (tik nauji)',
			    'Šios dienos' => 'Šios dienos',
			    'Vakar dienos' => 'Vakar dienos',
			    'Trijų dienų' => 'Trijų dienų',
			    'Savaitės' => 'Savaitės',
			    'Dviejų savaičių' => 'Dviejų savaičių',
		    ];   

    // Result ordering
    protected $order = [
                'Naujausi viršuje' => 'Naujausi viršuje',
                'Pigiausi viršuje' => 'Pigiausi viršuje',
                'Brangiausi viršuje' => 'Brangiausi viršuje'
            ]; 

	/**
	 * Class constructor.
	 */
	public function __construct() {
		// Get car makes
		$this->makes = ['' => 'Visos'] + CarMake::all()->pluck('name', 'name')->toArray();		

        // Generate every year from 1990 till current
        $dynamic_years = array_combine(range(1990, date('Y')), range(1990, date('Y')));
        // Join dynamic and static years
        $this->years = ['' => ''] + $dynamic_years + $this->years;
	}

	/**
	 * Return options object.
	 * 
	 * @return object
	 */
	public static function get() {
		// Get class context from static method
		$self = new static;

		return (object) [
			'makes'  => $self->makes,
			'years'  => $self->years,
			'prices' => $self->prices,
			'fuel' 	 => $self->fuel,
			'age' 	 => $self->age,
            'order'  => $self->order
		];
	}
}