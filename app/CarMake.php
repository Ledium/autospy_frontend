<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarMake extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Make has many models.
     * 
     * @return has-many
     */
    public function models()
    {
        return $this->hasMany('\App\CarModel');
    }

}
