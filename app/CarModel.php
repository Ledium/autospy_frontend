<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarModel extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'car_make_id'
    ];

    /**
     * Model belongs to single make.
     * 
     * @return belongs-to
     */
    public function make()
    {
        return $this->belongsTo('\App\CarMake', 'car_make_id');
    }

}
