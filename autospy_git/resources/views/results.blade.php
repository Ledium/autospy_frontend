@extends('layouts.app', ['title' => 'Paieškos rezultatai'])

@section('content')

<div class="panel-body">

	<div class="row">
		@if(count($items) > 0)
		{{-- Output all results --}}
		@foreach($items as $item)

		<div class="col-sm-6 col-md-4">
			<a target="_blank" href="{{ $item->url }}">
				<div class="thumbnail ad">
					<img src="{{ $item->image }}">
					<div class="caption">
						<h3>{{ $item->title }}</h3>
						<table>
							<tbody>
								<tr>
									<td>
										Pagaminimo data:
									</td>
									<td>
										{{ $item->year }}
									</td>
								</tr>
								<tr>
									<td>
										Kaina:
									</td>
									<td>
										{{ $item->price }} &euro;
									</td>
								</tr>
								<tr>
									<td>
										Miestas:
									</td>
									<td>
										{{ $item->city }}
									</td>
								</tr>
								<tr>
									<td>
										Kuro tipas:
									</td>
									<td>
										{{ $item->fuel }}
									</td>
								</tr>	
								<tr>
									<td>
										Pavarų dėžė:
									</td>
									<td>
										{{ $item->gearbox }}
									</td>
								</tr>
								<tr>
									<td>
										Variklis:
									</td>
									<td>
										{{ $item->engine }}{{ $item->power ? ', ' . $item->power : '' }}
									</td>
								</tr>
							</tbody>
						</table>
						<img class="img img-responsive" src="{{ asset('img/' . $item->vendor . '.png') }}" alt="{{ ucfirst($item->vendor) }}">
					</div>
				</div>
			</a>
		</div>

		@endforeach
		@else
		{{-- No results --}}
		<div class="col-lg-12 text-center">
			<h3>Pagal paieškos kriterijus rezultatų nerasta</h3>
		</div>
		@endif
	</div>
</div>

@endsection
