<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Netinka live search, darom su scrappinimu į mūsų DB

Route::get('/home', function() {
	return redirect()->route('home');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/rezultatai', 'HomeController@search')->name('search');
Route::post('/api/models', 'HomeController@getModels')->name('api.models');
