<?php
/**
 * Content scrapper abstract (template) class.
 */
namespace App\Autospy;

use Goutte;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

abstract class Driver
{
	// Vendor filter keys (application key => vendor key)
	protected $key_bindings = [];

	// Vendor filter values (application value => vendor value)
	protected $value_bindings = [];

	// Custom log handler. Creates separate log per vendor.
	protected $log;

	// Vendor name
	public $vendor = 'temp';

	// Vendor search query URL
	protected $search_url;

	// CSS selector-style filtering query for Crawler
	protected $selector = '.all-ads-block .item-link';

	/**
	 * Class constructor.
	 */
	public function __construct() {
		// Initialize custom log handler for this vendor
		$this->log = new Logger(strtoupper($this->vendor));
        $this->log->pushHandler(new StreamHandler(storage_path() . '/logs/' . $this->vendor . '.log', Logger::INFO));
	}

	/**
	 * Initialize Crawler object.
	 * 
	 * @param  array  $filter (optional) Filtering query
	 * @return Goutte		  DOM parser (crawler)
	 */
	protected function createCrawler($filter = null) {
		// Adjust filter if necessary
		$filter = $filter ? $this->adaptFilter($filter) : [];
		
		// Initialie Goutte client
		$client = new Goutte\Client();
		// Impersonate regular browser
		$client->setHeader('user-agent', "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36");
		
		// Get remote HTML
		$crawler = $client->request('GET', $this->search_url . '?' . http_build_query($filter));

		return $crawler;
	}

	/**
	 * Perform search task on this vendor.
	 * 
	 * @param  array $filter (optional) Filtering query
	 * @return array 		 Resulting array with Ad objects
	 */
	public static function search($filter = null) {

		// Initialize class context from static method
		$self = new static;
		$self->log->addInfo('Initial filter: ' . json_encode($filter));

		// Initialize Crawler
		$crawler = $self->createCrawler($filter);
	    
	    // Query crawled HTML
	    $results = $crawler->filter($self->selector)->each(function ($node) use ($self) {
	    	// Extract data from each node to Ad object
	    	return $self->extractData($node);
	    });

	    $self->log->addInfo('Total scrapped results: ' . count($results));

	    return $results;
	}

	/**
	 * Create Ad object from single result node.
	 * 
	 * @param  DOMElement $node Single crawled DOM element
	 * @return Ad 				Standardized result object
	 */
	abstract protected function extractData($node);

	/**
	 * Change application's filter to meet vendors filter.
	 * 
	 * @param  array $filter (optional) Filter query parameters
	 * @return array 		 Filter with updated structure       
	 */
	protected function adaptFilter($filter = null) {
		
		// Make sure that blank values are also in filter. Otherwise array_combine will trow error.
		$filter['model'] = isset($filter['model']) ? $filter['model'] : '';
		$filter['text'] = isset($filter['text']) ? $filter['text'] : '';
		$filter['age'] = isset($filter['age']) ? $filter['age'] : '';
		$filter['order'] = isset($filter['order']) ? $filter['order'] : '';
		
		// Convert application filter values to vendor values
		foreach($filter as $key => $item) {
			// Check if binding exists for current filter value
			if(array_key_exists($key, $this->value_bindings) && array_key_exists($item, $this->value_bindings[$key])) {
				// Update filter value from value binding array
				$filter[$key] = $this->value_bindings[$key][$item];
			}

			// Check if binding exists for current filter key
			if(array_key_exists($key, $this->key_bindings)) {
				// Update filter value from value binding array
				$filter[$this->key_bindings[$key]] = $filter[$key];
				unset($filter[$key]);
			}
		}

		$this->log->addInfo('Adapted filter: ' . json_encode($filter));

		return $filter;
	}

	/**
	 * Get latest ads.
	 * 
	 * @param  integer $limit (optional) Limit number of results
	 * @return array       
	 */
	public static function getLatest($limit = null) {
		// Sort by latest creation date
		$filter = [
			'age' => 'Rodyti visus',
			'order' => 'Naujausi viršuje'
		];

		$results = (new static)->search($filter);

		return $limit ? array_slice($results, 0, $limit) : $results;
	}
}