<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Autospy\Autogidas;
use App\CarMake;
use App\CarModel;
use App\Options;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $latest = Autogidas::getLatest(2);

        $options = Options::get();

        return view('home', compact('options', 'latest'));
    }

    /**
     * Scrap content using search query.
     * 
     * @param  Request $request 
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request) {
        $filter = $request->query();

        $items = Autogidas::search($filter);

        return view('results', compact('items'));
    }

    // API
    
    /**
     * Get models for car make.
     * 
     * @param  Request $request
     * @return array
     */
    public function getModels(Request $request) {
        $make = CarMake::whereName($request->make)->first();

        if($make) {
            $models = $make->models()->pluck('name');
            return $models;
        } else {
            throw new \Illuminate\Database\Eloquent\ModelNotFoundException;
        }
    }
}
