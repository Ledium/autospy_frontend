/**
 * Update model dropdown with provided data.
 * 
 * @param  {array} data
 * @return {void}
 */
function loadModels(data) {
	// Grab DOM element
	var dropdown = $('select[name=model]');
	// Empty options
	dropdown.empty();

	if(data && data.length > 0) {
		// Add 'show all' option
		dropdown.append('<option value="">Visi</option>');

		// Rebuild new options
		for (var i = 0; i < data.length; i++) {
			dropdown.append('<option value="' + data[i] + '">' + data[i] + '</option>');
		}

		// Make sure that dropdown isn't disabled
		dropdown.removeAttr('disabled');
	} else {
		// Disable empty dropdown
		dropdown.attr('disabled', 'disabled');		
	}
}

$(document).ready(function() {
	
	// Get models on make change
	$('select[name=make]').change(function() {

		// Clear model dropdown if 'show all'
		if(!$(this).val()) {
			loadModels();
			return;
		}

		$.ajax({
			url: window.Laravel.api.models,
			method: 'POST',
			data: {
				_token: window.Laravel.csrfToken,
				make: $(this).val()
			},
			success: function(data) {
				loadModels(data);
			},
			error: function(response) {
				console.log('Error', response);
			}
		})
	})
	
})