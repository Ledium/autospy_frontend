@include('layouts.header')

@yield('styles')

@yield('content')

@include('layouts.footer')