<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>AutoSpy</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    
    <link href="{{ asset('css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    

<!-- Optional theme -->


    <!-- Scripts -->

        
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'api' => [
                'models' => route('api.models')
            ]
        ]) !!};

    </script>
     
    
    

</head>
<body>
   <div id="app">

             <div class="header-2">
  <div class="header-2__top" data-aos="fade-up">
    <div class="header-2__top__container">
      <div class="header-2__top__wrap"><a class="header-2__logo" href="/">
          <img src="img/logo.png" alt="Mountain View"></a>
      </div>
       @if (Auth::guest())
      <div class="header-2__account"><a class="header-2__account__log-in" href="{{ route('login') }}">Prisijungti</a><a class="header-2__account__sign-up" href="{{ route('register') }}">Registruotis</a></div>
        @else
         <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                        </div>
                        </div>
    
      

       









