
					</div>
		        </div>
		    </div>
		</div>

    </div>
    <div class="footer-2">
  <div class="footer-2__left credentials"><a class="footer-2__link" href="#">© 2017 AutoSpy.lt</a>
    
  </div>
  <div class="footer-2__right privacy">
   <a class="footer-2__link" href="#">Privatumo politika</a>
  </div>
</div>

    <!-- Scripts -->
       <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('/js/vendors.js') }}"></script>
    <script src="{{ asset('/js/main.js') }}"></script>
    <script src="{{ asset('/js/select2.js') }}"></script>
    <script src="{{ asset('/js/grid.js') }}"></script>
       <script src="{{ asset('/js/jquery.nicescroll.js') }}"></script>


    <script>
  
    $('select').select2();

    
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-angle-down arrow"></i>');

 $(document).ready(function() {  
    $("html").niceScroll();
    $("body").css({"overflow-x": "hidden"});
});
    </script>


	
	{{-- View based scripts --}}
    @yield('scripts')
</body>
</html>