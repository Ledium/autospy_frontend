@extends('layouts.app', ['title' => 'Home page'])

</div>

@section('content')


 <div class="header-2__bottom" data-aos="fade-up">
    <div class="header-2__bottom__container container">
      <h1 class="header-2__title">Automobilių skelbimų paieška</h1>
      <div class="header-2__subheading subheading">Automatinė skelbimų paieška didžiausiuose automobilių pardavimo portaluose</div>


        <fieldset class="header-2__form">  
        {!! Form::open(['url' => route('search'), 'method' => 'GET']) !!}
        <div class="">
        <div class="header-2__form__textbox textbox">
      
            {!! Form::select('make', $options->makes, null, ['id' => 'makes','title' => 'Markė', 'class' => 'input']) !!}
        </div>
         <div class="header-2__form__textbox textbox">
        
            {!! Form::select('model', [], null, ['class' => 'input', 'disabled' => 'disabled']) !!}
          </div>
          </div>
        <div class="">
        <div class="header-2__form__textbox textbox year_from">
          
            {!! Form::select('year_from', $options->years, null, ['class' => 'input']) !!}
        </div>
        <div class="rectangle">
        </div>
         <div class="header-2__form__textbox textbox">
        
            {!! Form::select('year_to', $options->years, null, ['class' => 'input']) !!}
          </div>
          <div class="header-2__form__textbox textbox year_from">
          
            {!! Form::select('price_from', $options->prices, null, ['class' => 'input']) !!}
          </div>
          <div class="rectangle">
        </div>
          <div class="header-2__form__textbox textbox">
        
            {!! Form::select('price_to', $options->prices, null, ['class' => 'input']) !!}
          </div>
          </div>
          <div class="">
          <div class="header-2__form__textbox textbox extra_margin">
         
            {!! Form::select('fuel', $options->fuel, null, ['class' => 'input']) !!}
        </div>
         <div class="header-2__form__textbox textbox text_search">
          
            {!! Form::text('text', null, ['placeholder' => 'tekstinė paieška', 'class' => 'input_large']) !!}
          </div>
          <div class="header-2__form__textbox textbox">
           
            {!! Form::select('age', $options->age, null, ['class' => 'input']) !!}
          </div>
          </div>
          <div class="">
          <div class="header-2__form__textbox textbox text_search">
           {!! Form::submit('Ieškoti', ['class' => 'subscribe-1__form__button button submit_button']) !!}

          {!! Form::close() !!}
          </div>
          </div>
               </fieldset>
          </div>
          </div>
        
    </div>
  </div>
  </div>
  </div>
  @if(count($latest) > 0)
	    <fieldset class="auto_fieldset">

<div class="container auto">
<h2 class="newest_auto"> Naujausi skelbimai </h2>
            <div class="row">
            @foreach($latest as $item)
					<div class="col-sm-6 col-md-4">
					<a target="_blank" href="{{ $item->url }}">
					   

					  <div class="block">
					    <div class="photo">
					    <img class ="photo_display" src="{{ $item->image }}">
					    </div>
					    <div class="auto_body">
					    	<h3 class="auto_title"> {{ $item->title }}, {{$item->price}}&euro;</h3>
					    	<p class="auto_description"> {{ $item->year }},{{ $item->city }}, {{ $item->fuel }}, {{ $item->gearbox }}, {{ $item->engine }}{{ $item->power ? ', ' . $item->power : '' }} </p>
					    	<img class="img img-responsive vendor" src="{{ asset('img/' . $item->vendor . '.png') }}" alt="{{ ucfirst($item->vendor) }}">
					    	</div>
					    </div>
					    </a>
					    </div>
					      @endforeach
					     
					    </div>
					    </div>
			</fieldset>		 

   @endif


		<div class="subscribe-2">
  <div class="subscribe-2__container container">
    <h1 class="subscribe-2__title" data-aos="fade-up">Registruokis</h1>
    <div class="subscribe-2__subheading subheading" data-aos="fade-up">ir gauk pranešimus apie atitinkančius jūsų kriterijus naujus skelbimus tiesiai į el. paštą</div>
    <form class="subscribe-2__form" data-aos="fade-up">
      <div class="subscribe-2__form__field">
        <div class="subscribe-2__form__textbox textbox">
          <input class="textbox__input" type="text" placeholder="Vardas" required="required"/>
        </div>
      </div>
      <div class="subscribe-2__form__field">
        <div class="subscribe-2__form__textbox textbox">
          <input class="textbox__input" type="email" placeholder="El paštas" required="required"/>
        </div>
      </div>
      <div class="subscribe-2__form__field">
        <div class="subscribe-2__form__textbox textbox textbox--password">
          <input class="textbox__input" type="password" placeholder="Slaptažodis" required="required"/>
          <button class="textbox__toggle" type="button">
            <svg class="icon textbox__toggle__icon">
              <use xlink:href="#svg-icon-eye"></use>
            </svg>
          </button>
        </div>
      </div>
      <div class="subscribe-2__form__field">
        <button class="subscribe-2__form__button button" type="submit">Registruotis</button>
      </div>
    </form>
  </div>
</div>



@endsection
