@extends('layouts.app', ['title' => 'Paieškos rezultatai'])

@section('content')
</div>

<div class="container_fluid">
<div class="result_search" data-aos="fade-down">
  	<div class="row">
  	<div class="make col-md-1">
  	</div>
  		<div class="make col-md-2">
	  		<div class="result_title_search">Markė: </div>
  		</div>
  		<div class="make col-md-2">
  			<div class="result_title_search">Modelis: </div>
  		</div>
  		<div class="make col-md-3">
  			<div class="result_title_search">Metai</div>
  		</div>
  		<div class="make col-md-3">
  		<div class="result_title_search">kaina &euro;</div>
  		</div>
  		<div class="make col-md-1">
  	</div>
  	</div>
  	<div class="clearfix"></div>
  	<div class="row">
  	<div class="make col-md-1">
  	</div>
  		<div class="make col-md-2">
	  		<div class="result_title_search_long">kuro tipas</div>
  		</div>
  		<div class="make col-md-3">
  			<div class="result_title_search_long">Paskelbta prieš</div>
  		</div>
  		<div class="make col-md-4">
  			<div class="result_title_search_long">Tekstinė paieška</div>
  		</div>
  		<div class="make col-md-1">
  		<div class="pull-left gridlist btn-group span2">
  		<a href="#" id="grid" class="  btn btn-primary selection"><span class="fa fa-th-large "></span></a>
<a href="#" id="list" class="  btn btn-default selection"><span class="fa fa-list-ul "></span></a>

</div>
  		
  		</div>
  		<div class="make col-md-1">
  	</div>
  	</div>
  </div>
  </div>
     <fieldset class="auto_fieldset">
 <div class="auto_container" data-aos="fade-down">
  <div class="row ">
  <div class="col-md-offset-1"></div>
  <div class="col-md-10">
  
  		@if(count($items) > 0)
		{{-- Output all results --}}
							    	
 


		
		@foreach($items as $item)


  				<div class="col-md-3 car_result">

					<a target="_blank" href="{{ $item->url }}">
					   

					  <div class="blocki block_result">
					   
					    <img class ="photo_display" src="{{ $item->image }}">
					    
					    <div class="auto_body_result">
					    	<div class="first_line ">
					    		<h3 class="auto_title"> {{ $item->title }} </h3>
					    		<h3 class="auto_price"> {{$item->price}}&euro; </h3>
					    </div>	
					    <div class="second_line">
					    		<p class="auto_description"> <span class="date"><span class="no_show"> Pagaminimo data:</span> {{ $item->year }}</span> <span class="miles"><span class="no_show"> Miestas:</span> {{ $item->city }}</span><span class="engine"><span class="no_show">  Variklis:</span> {{ $item->engine }}{{ $item->power ? ', ' . $item->power : '' }}</span></p>
					    </div>
					    <div class="third_line">
					    		<p class="auto_description"> <span class="date"><span class="no_show"> Kuro tipas:</span>{{ $item->fuel }} </span> <span class="gear"><span class="no_show"> Pavarų dežė:</span> 	{{$item -> gearbox}}</span></p>
					    </div>
					    		<img class="img img-responsive vendor_result" src="{{ asset('img/' . $item->vendor . '.png') }}" alt="{{ ucfirst($item->vendor) }}">
					   </div>
					 </div>
					</a>


				</div>
					    
	 
					 

		@endforeach
		 
						</div>
</div>
    </div>
    </fieldset>	

					
			
		
		 @else
		{{-- No results --}}
		<div class="col-lg-12 text-center">
			<h3>Pagal paieškos kriterijus rezultatų nerasta</h3>
		</div>
		@endif 



@endsection
